package group_iterator;

public class Queue implements QueueInterface {

	private int[] storage;
	private int counter;

	/**
	 * Constructor mit einem Wert fuer die Defaultgroesse der Queue
	 * 
	 * @param size
	 *            default Groesse der Queue
	 */
	public Queue(int size) {
		storage = new int[size];
	}

	/**
	 * Constructor mit Defaultgroesse 5
	 */
	public Queue() {
		storage = new int[5];
	}

	/**
	 * Main methode welche ein Paar elemente hinzufuegt und dann wieder abfragt
	 * und am Ende ueberprueft ob die queue leer ist.
	 */
	public static void main(String[] args) {
		Queue myqu = new Queue();
		myqu.addElement(5);
		System.out.println(myqu.removeElement());
		myqu.addElement(3);
		myqu.addElement(14);
		System.out.println(myqu.removeElement());
		myqu.addElement(3);
		myqu.addElement(14);
		myqu.addElement(17);
		myqu.removeElement();
		myqu.removeElement();
		myqu.removeElement();
		System.out.println(myqu.removeElement());
		System.out.println("Ist leer:" + myqu.isEmpty());
	}

	/**
	 * Fuegt Element zur Queue hinzu
	 * 
	 * @param e
	 *            Wert der hinzugefuegt wird
	 */
	@Override
	public void addElement(int e) {
		makeSuitableArray();
		for (int i = 0; i < storage.length; i++) {
			if (storage[i] == 0) {
				storage[i] = e;
				counter++;
				return;
			}
		}
	}

	/**
	 * Gibt das letzte Element zurueck aus der Queue
	 * 
	 * @return int Rueckgabewert der Queue
	 */
	@Override
	public int removeElement() {
		for (int i = 0; i < storage.length; i++) {
			if (storage[i] != 0) {
				counter--;
				int value = storage[i];
				storage[i] = 0;
				return value;
			}
		}
		return 0;
	}

	/**
	 * Schaut ob Speicher leer ist
	 * 
	 * @return boolean Gibt true zurueck wenn er leer ist
	 */
	@Override
	public boolean isEmpty() {
		for (int i : storage) {
			if (i != 0) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Gibt das ganze Array zurueck (wird beim testing verwendet)
	 * 
	 * @return int[] queue array
	 */
	public int[] getStorage() {
		return storage;
	}

	// private methods
	/**
	 * Ueberprueft ob das storage array noch platz hat und erstellt bei Bedarf
	 * ein doppelt so grosses array und uebertraegt die Werte
	 */
	private void makeSuitableArray() {
		if (isArrayFull()) {
			int[] backup_storage = storage;
			storage = new int[storage.length * 2];
			for (int i = 0; i < backup_storage.length; i++) {
				storage[i] = backup_storage[i];
			}
		}
	}

	/**
	 * Schaut ob das Array voll ist
	 * 
	 * @return boolean ist voll
	 */

	private boolean isArrayFull() {
		return (storage.length == counter) ? true : false;
	}
}