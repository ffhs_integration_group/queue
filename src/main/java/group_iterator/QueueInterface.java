package group_iterator;

public interface QueueInterface {
	
	void addElement(int e);
	int removeElement();
	boolean isEmpty();

}
