package group_iterator;

import static org.junit.Assert.*;

import org.junit.Test;

public class QueueTest {
	
	@Test
	public void testAddElement() {
		Queue q1 = new Queue();
		q1.addElement(1);
		q1.addElement(2);
		int[] s = q1.getStorage();
		assertEquals("didn't add number to the right place",s[1],2);
	}
	
	@Test
	public void testRemoveElement() {
		Queue q1 = new Queue();
		q1.addElement(1);
		q1.addElement(2);
		q1.removeElement();
		int e2 = q1.removeElement();
		assertEquals("didn't get the right number",e2,2);
	}
	
	@Test
	public void testisEmpty() {
		Queue q1 = new Queue();
		q1.addElement(1);
		q1.addElement(2);
		q1.removeElement();
		q1.removeElement();
		assertTrue(q1.isEmpty());
	}
	
	@Test
	public void testGetStorage() {
		Queue q1 = new Queue();
		q1.addElement(1);
		int[] s = q1.getStorage();
		assertEquals(s[0],1);
	}
	
	@Test
	public void testAddElementsWhenFull() {
		Queue q1 = new Queue(2);
		q1.addElement(1);
		q1.addElement(2);
		q1.addElement(3);
		q1.removeElement();
		q1.removeElement();
		int e = q1.removeElement();
		assertEquals("didn't add element",e, 3);
	}
	
	@Test
	public void testGrowth() {
		Queue q1 = new Queue(2);
		q1.addElement(1);
		q1.addElement(2);
		q1.addElement(3);
		assertEquals("didn't add element",q1.getStorage().length, 4);
	}
}
